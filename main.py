from fastapi import FastAPI,Response
import uvicorn
import matplotlib.pyplot as plt
import io
import numpy as np
import random
a = np.arange(100) 
b = np.sqrt(a) 
c = np.exp(a)
d = np.log(a)
e = np.sin(a)
dt = {}
dt['b'] = {'lt': b, 'sty': '-', 'clr':'g'}
dt['c'] = {'lt': c, 'sty': '--', 'clr':'r'}
dt['d'] = {'lt': d, 'sty': '-.', 'clr':'y'}
dt['e'] = {'lt': e, 'sty': ':', 'clr':'b'}
app = FastAPI()

@app.get("/")
def homepage():
    body_str = '''
    <a href=http://localhost:7000/>http://localhost:7000/</a><br/>
    <a href=http://localhost:7000/111>http://localhost:7000/111</a><br/>
    <a href=http://localhost:7000/222>http://localhost:7000/222</a><br/>
    <a href=http://localhost:7000/111_massive>http://localhost:7000/111_massive</a><br/>
    <a href=http://localhost:7000/222_massive>http://localhost:7000/222_massive</a><br/>
    '''
    return Response(body_str, media_type="text/html")

@app.get("/111")
def t111():
    
    # 1
    key = random.choice(['b','c','d','e'])
    plt.plot(a, dt[key]['lt'], label=key, linestyle=dt[key]['sty'], color=dt[key]['clr'])
    
    my_stringIObytes = io.BytesIO()
    plt.savefig(my_stringIObytes, format='png')
    my_stringIObytes.seek(0)
    my_bytes_pngData = my_stringIObytes.read()
    plt.close()
    return Response(my_bytes_pngData, media_type="image/png")
    

@app.get("/222")
def t222():
    
    # 2
    key = random.choice(['b','c','d','e'])
    fig, ax = plt.subplots() # fig : figure object, ax : Axes object
    ax.plot(a, dt[key]['lt'], label=key, linestyle=dt[key]['sty'], color=dt[key]['clr'])
    
    my_stringIObytes = io.BytesIO()
    fig.savefig(my_stringIObytes, format='png')
    my_stringIObytes.seek(0)
    my_bytes_pngData = my_stringIObytes.read()
    plt.close()
    return Response(my_bytes_pngData, media_type="image/png")


@app.get("/111_massive")
def t111_massive():
    body_str = ''
    for i in range(4):
        for j in range(10):
            body_str += "<img src=http://localhost:7000/111?" + str(random.random()) + " /></a>\n"
            pass 
        body_str += "<br/>\n"
    return Response(body_str, media_type="text/html")

@app.get("/222_massive")
def t222_massive():
    body_str = ''
    for i in range(4):
        for j in range(10):
            body_str += "<img src=http://localhost:7000/222?" + str(random.random()) + " /></a>\n"
            pass 
        body_str += "<br/>\n"
    return Response(body_str, media_type="text/html")

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=7000)
