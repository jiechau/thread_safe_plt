FROM python:3.11-slim-buster
WORKDIR /app
ADD . /app
RUN pip3 install -r requirements.txt
EXPOSE 8000
CMD ["python", "main.py"]

